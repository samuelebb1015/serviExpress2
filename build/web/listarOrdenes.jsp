<%-- 
    Document   : listarOrdenes
    Created on : 03-jul-2018, 5:44:25
    Author     : Samuel
--%>

<%@page import="serviexpress.complementos.negocio.DetallePedido"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Órdenes</title>
    </head>
    <body>
        <table align="center" border="1px" width="80%">
           
            

            <tr>
                <th>ID</th>
                <th>Fecha/Hora</th>
                <th>Proveedor</th>
                <th>Empleado</th>
                <th>Detalle</th>
                <th>Acción<th>
            </tr>
            <%
                String prov = "";
                Pedido pedido = new Pedido();
                ArrayList<Pedido> lista = pedido.listarOrdenesPedidos();
                for(Pedido p: lista){              
                            
                %>
        
                <tr>
                    <td><%=p.getId()%> </td>
                    <td><%=p.getFecha()%> </td>
                    <td><%=p.getIdProv()%> </td>
                    <td><%=p.getIdEmp()%> </td>                    
                    <td><a href="listarDetalle.jsp?idPro=<%=p.getId()%>">Detalle</a></td>                    
                </tr>
        <% 
            }
        
        %>
        </table>
    </body>
</html>
