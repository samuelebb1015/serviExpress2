

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <h1>Registrar Cliente</h1>

        <form action="autoRegistroClienteController" method="POST" >

            Rut <input type="text" name="txtRut" maxlength="12" required/><br>
            Primer Nombre 
            <input type="text" name="txtNombre1" required/><br>
            Segundo Nombre 
            <input type="text" name="txtNombre2" ><br>
            Apellido Materno 
            <input type="text" name="txtApellido1" required/><br>
            ApellidoPaterno 
            <input type="text" name="txtApellido2" ><br>
            Telefono 
            <input type="text" name="txtTelefono" maxlength="9" required/><br>
            Dirección 
            <input type="text" name="txtDireccion" required/><br>
            Email 
            <input type="text" name="txtEmail" required/><br>
            username 
            <input type="text" name="txtUser" required/><br>
            password 
            <input type="password" name="txtPass" required/><br>

            <input type="submit" name="btnRegistrar" value="Registrar">

        </form>

        <%
            out.print("<br>");
            out.print(request.getAttribute("resultado"));

            if (request.getAttribute("resultado") == "0") {
                out.print("<br><h2>Cliente Registrado Exitosamente</h2>");
            }

            if (request.getAttribute("resultado") == "1") {
                out.print("<br><h2>ERROR : Cliente No se pudo registrar");
            }
        %>

    </body>
</html>
