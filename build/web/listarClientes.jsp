<%-- 
    Document   : listarClientes
    Created on : 28-may-2018, 1:33:58
    Author     : Samuel
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Clientes</title>
    </head>
    <body>
        <table align="center" border="1px" width="80%">
            <tr>
                <th>ID</th>
                <th>Rut</th>
                <th>Primer Nombre</th>
                <th>Segundo Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Dirección</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Sucursal ID</th>
                <th>Username</th>
                <th>Fiado</th> 
                <th>Editar</th>
            </tr>
            <%
                String fiado = "";
                Cliente cliente = new Cliente();
                ArrayList<Cliente> lista = cliente.listarClientes(1);
                for(Cliente c: lista){
               
                if(c.getFiado()== 1){
                    fiado = "Si";
                }else{
                    fiado = "No";
                }
                %>
        
                <tr>
                    <td><%=c.getId() %> </td>
                    <td><%=c.getRut()%> </td>
                    <td><%=c.getPrimerNombre()%> </td>
                    <td><%=c.getSegundoNombre()%> </td>
                    <td><%=c.getApellidoPaterno()%> </td>
                    <td><%=c.getApellidoMaterno()%> </td>
                    <td><%=c.getDireccion() %> </td>
                    <td><%=c.getEmail()%> </td>
                    <td><%=c.getTelefono()%> </td>
                    <td><%=c.getSucursal() %> </td>
                    <td><%=c.getUsername()%> </td>
                    <td><%= fiado %> </td>
                    <td><a href="editarClientes.jsp?idCli2=<%=c.getId()%>">Editar</a></td>
                </tr>
        <% 
        }
        %>
        </table>
    </body>
</html>
