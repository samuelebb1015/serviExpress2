<%-- 
    Document   : listarClientes
    Created on : 28-may-2018, 1:33:58
    Author     : Samuel
--%>
<%@page import="serviexpress.complementos.negocio.TipoEmpleado"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Empleados</title>
    </head>
    <body>
               
        <table align="center" border="1px" width="80%">
            <tr>
                <th>ID</th>
                <th>Rut</th>
                <th>Primer Nombre</th>
                <th>Segundo Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Dirección</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Sucursal</th>
                <th>Username</th>
                <th>Tipo Empleado</th> 
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
            <%
                String t="";
                Empleado empleado = new Empleado();
                TipoEmpleado tipo = new TipoEmpleado();
                ArrayList<Empleado> lista = empleado.listarEmpleados(1);
                ArrayList<TipoEmpleado> list = tipo.listarTipoEmpleado();
                for(Empleado e: lista){
                  if(e.getTipo()==1){
                       t ="Administrador";
                  }if(e.getTipo()==2){
                       t = "Mecánico";
                  }if(e.getTipo()==3){
                      t="Cajero";
                  }
                %>
        
                <tr>
                    <td><%=e.getId()%> </td>
                    <td><%=e.getRut()%> </td>
                    <td><%=e.getPrimerNombre()%> </td>
                    <td><%=e.getSegundoNombre()%> </td>
                    <td><%=e.getApellidoPaterno()%> </td>
                    <td><%=e.getApellidoMaterno()%> </td>
                    <td><%=e.getDireccion() %> </td>
                    <td><%=e.getEmail()%> </td>
                    <td><%=e.getTelefono()%> </td>
                    <td><%= "ServiExpress" %> </td>
                    <td><%= e.getUsername()%> </td>
                    <td><%= t %> </td>
                    <td><a href="editarEmpleado.jsp?idEmp=<%=e.getId()%>">Editar</a></td>
                    <td><a href="listarEmpleados.jsp?idEmp=<%=e.getId()%>">Eliminar</a></td>
                    
                </tr>
        <% 
        }
        %>
        </table>
        
        <%  if(request.getParameter("idEmp")!=null){
            Empleado emp = new Empleado();
            int cod = Integer.parseInt(request.getParameter("idEmp"));
            emp.bajaEmpleado(cod, 1);
            request.getRequestDispatcher("listarEmpleados.jsp");
            }
        
        %>
        
      </body>
      
      
</html>
