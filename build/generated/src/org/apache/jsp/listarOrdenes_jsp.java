package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import serviexpress.complementos.negocio.DetallePedido;
import java.io.*;
import java.util.*;
import serviexpress.persistencia.*;

public final class listarOrdenes_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Listar Órdenes</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <table align=\"center\" border=\"1px\" width=\"80%\">\n");
      out.write("           \n");
      out.write("            \n");
      out.write("\n");
      out.write("            <tr>\n");
      out.write("                <th>ID</th>\n");
      out.write("                <th>Fecha/Hora</th>\n");
      out.write("                <th>Proveedor</th>\n");
      out.write("                <th>Empleado</th>\n");
      out.write("                <th>Detalle</th>\n");
      out.write("                <th>Acción<th>\n");
      out.write("            </tr>\n");
      out.write("            ");

                String prov = "";
                Pedido pedido = new Pedido();
                ArrayList<Pedido> lista = pedido.listarOrdenesPedidos();
                for(Pedido p: lista){              
                            
                
      out.write("\n");
      out.write("        \n");
      out.write("                <tr>\n");
      out.write("                    <td>");
      out.print(p.getId());
      out.write(" </td>\n");
      out.write("                    <td>");
      out.print(p.getFecha());
      out.write(" </td>\n");
      out.write("                    <td>");
      out.print(p.getIdProv());
      out.write(" </td>\n");
      out.write("                    <td>");
      out.print(p.getIdEmp());
      out.write(" </td>                    \n");
      out.write("                    <td><a href=\"listarDetalle.jsp?idPro=");
      out.print(p.getId());
      out.write("\">Detalle</a></td>                    \n");
      out.write("                </tr>\n");
      out.write("        ");
 
            }
        
        
      out.write("\n");
      out.write("        </table>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
