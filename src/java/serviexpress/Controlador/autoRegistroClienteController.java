/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import serviexpress.persistencia.Cliente;

/**
 *
 * @author jhan
 */
@WebServlet(name = "autoRegistroClienteController", urlPatterns = {"/autoRegistroClienteController"})
public class autoRegistroClienteController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }


     
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        RequestDispatcher res;
        if(request.getParameter("btnRegistrar")!=null){    
        //Rescate de variables del Formulario
        String rut = request.getParameter("txtRut");
        String nombre1 = request.getParameter("txtNombre1");
        String nombre2 = request.getParameter("txtNombre2");
        String apellido1 = request.getParameter("txtApellido1");
        String apellido2 = request.getParameter("txtApellido2");
        int telefono = Integer.parseInt(request.getParameter("txtTelefono"));
        String direccion = request.getParameter("txtDireccion");
        String email = request.getParameter("txtEmail");
        String username = request.getParameter("txtUser");
        String pass = request.getParameter("txtPass");
        
        //Creacion de objeto para cargar datos
        Cliente objCliente = new Cliente();
        
        //Seteo de parametros a objeto
        objCliente.setSucursal(1);//Cargamos el valor ID de Serviexpress
        objCliente.setRut(rut);
        objCliente.setPrimerNombre(nombre1);
        objCliente.setSegundoNombre(nombre2);
        objCliente.setApellidoMaterno(apellido1);
        objCliente.setApellidoPaterno(apellido2);
        objCliente.setTelefono(telefono);
        objCliente.setDireccion(direccion);
        objCliente.setEmail(email);
        objCliente.setUsername(username);
        objCliente.setPassword(pass);
        
//Creamos el objeto;
int resultado = objCliente.crearCliente(objCliente);
request.setAttribute("resultado", resultado); 


            res=request.getRequestDispatcher("autoRegistrarCliente.jsp");
            res.forward(request, response);

    }
        
    }
    }

   
}
