/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import serviexpress.complementos.negocio.DetallePedido;


/**
 *
 * @author Samuel
 */
@WebServlet(name = "generarOrdenDetalleController", urlPatterns = {"/generarOrdenDetalleController"})
public class generarOrdenDetalleController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet generarOrdenDetalleController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet generarOrdenDetalleController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        RequestDispatcher res;
        if(request.getParameter("btnAgregar")!=null){    
        //Rescate de variables del Formulario
        
        int cantidad = Integer.parseInt(request.getParameter("txtCantidad"));     
        int idProd = Integer.parseInt(request.getParameter("producto")); 
        int idOrden = Integer.parseInt(request.getParameter("txtOrden"));
        
        
        
        //Creacion de objeto para cargar datos
        DetallePedido objDetalle = new DetallePedido();
        
        //Seteo de parametros a objeto
        objDetalle.setCantidad(cantidad);
        objDetalle.setIdProd(idProd); 
        objDetalle.setIdOrden(idOrden);
        
        
        
//Creamos el objeto;
int resultado = objDetalle.agregarProductoDetallePedido(objDetalle);


            res=request.getRequestDispatcher("generarOrdenDetalle.jsp");
            res.forward(request, response);
    }
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
