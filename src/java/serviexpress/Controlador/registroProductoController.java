/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import serviexpress.persistencia.Producto;


/**
 *
 * @author Samuel
 */
@WebServlet(name = "registroProductoController", urlPatterns = {"/registroProductoController"})
public class registroProductoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet registroProductoController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet registroProductoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        RequestDispatcher res;
        if(request.getParameter("btnRegistrar")!=null){    
        //Rescate de variables del Formulario
        
        String nombre = request.getParameter("txtNombre");
        String descripcion = request.getParameter("txtDescripcion");        
        int precio = Integer.parseInt(request.getParameter("txtPrecio"));
        int stock = Integer.parseInt(request.getParameter("txtStock"));
        int critico = Integer.parseInt(request.getParameter("txtCritico"));
        String categoria = request.getParameter("categoria");
        String proveedor = request.getParameter("proveedor");
        
        String idProd = categoria+proveedor;
        
        int idCategoria = Integer.parseInt(categoria);
        int idProveedor = Integer.parseInt(proveedor);
        int idProducto = Integer.parseInt(idProd);
        
        //Creacion de objeto para cargar datos
        Producto objProducto = new Producto();
        
        //Seteo de parametros a objeto
        objProducto.setId(idProducto);
        objProducto.setNombre(nombre);
        objProducto.setDescripcion(descripcion);
        objProducto.setPrecioVenta(precio);
        objProducto.setStock(stock);
        objProducto.setStockCritico(critico);        
        objProducto.setIdCategoria(idCategoria);
        objProducto.setIdProveedor(idProveedor);        
        objProducto.setVencimiento("000000");
        
        
//Creamos el objeto;
int resultado = objProducto.crearProducto(objProducto);
request.setAttribute("resultado", resultado); 

            res=request.getRequestDispatcher("/registrarProducto.jsp");
            res.forward(request, response);
    }
        }
    }
        

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

        }
