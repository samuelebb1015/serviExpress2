
package serviexpress.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;

public class Sucursal {

private int id;
private String nombre;
private String rut;
private int telefono;
private int telefono1;
private String direccion;
private String email;
private String horaApertura;
private String horaCierre;

    public Sucursal() {
    }

    public Sucursal(int id, String nombre, String rut, int telefono, int telefono1, String direccion, String email, String horaApertura, String horaCierre) {
        this.id = id;
        this.nombre = nombre;
        this.rut = rut;
        this.telefono = telefono;
        this.telefono1 = telefono1;
        this.direccion = direccion;
        this.email = email;
        this.horaApertura = horaApertura;
        this.horaCierre = horaCierre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(int telefono1) {
        this.telefono1 = telefono1;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(String horaApertura) {
        this.horaApertura = horaApertura;
    }

    public String getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(String horaCierre) {
        this.horaCierre = horaCierre;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id=" + id + ", nombre=" + nombre + ", rut=" + rut + ", telefono=" + telefono + ", telefono1=" + telefono1 + ", direccion=" + direccion + ", email=" + email + ", horaApertura=" + horaApertura + ", horaCierre=" + horaCierre + '}';
    }

//Metodo que busca y retorna un objeto Sucursal enviando como parametro el id
    public Sucursal buscarPorID(int id){
        try {
                ArrayList<Sucursal> lista = new ArrayList<>();
                Sucursal sucursal = new Sucursal();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_sucursal_adm.sp_find_sucursal_id(?,?) }");
        cs.setInt(1, id);
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Condominio
                sucursal.setId(rs.getInt(1));
                sucursal.setNombre(rs.getString(2));
                sucursal.setRut(rs.getString(3));
                sucursal.setTelefono(rs.getInt(4));
                sucursal.setTelefono1(rs.getInt(5));
                sucursal.setDireccion(rs.getString(6));
                sucursal.setEmail(rs.getString(7));
                sucursal.setHoraApertura(rs.getString(8));
                sucursal.setHoraCierre(rs.getString(9));

            
    } // Cierre ciclo while
           
            return sucursal;
            
            } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };
    
    //Metodo sin parametros que devuelve un arreglo de sucursales
    public ArrayList<Sucursal> listarSucursales(){
         try {
                ArrayList<Sucursal> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_sucursal_adm.sp_list_sucursales(?) }");
        cs.registerOutParameter(1, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(1);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                Sucursal objSucursal = new Sucursal();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                objSucursal.setId(rs.getInt(1));
                objSucursal.setNombre(rs.getString(2));
                objSucursal.setRut(rs.getString(3));
                objSucursal.setTelefono(rs.getInt(4));
                objSucursal.setTelefono1(rs.getInt(5));
                objSucursal.setDireccion(rs.getString(6));
                objSucursal.setEmail(rs.getString(7));
                objSucursal.setHoraApertura(rs.getString(8));
                objSucursal.setHoraCierre(rs.getString(9));
                
                lista.add(objSucursal); //se carga el objeto seteado en el ciclo
            
    } // Cierre ciclo while
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };
    
    public int crearSucursal(Sucursal sucursal){
        Conector conector   = new Conector();
        Connection con      = conector.connectar();
        
        int cod_error       = 1122;//Codigo ERROR: error ejecucion del procedimeinto almacenado
        
        //extraccion de datos del objeto enviado
        String vnombre       = sucursal.getNombre();
        String vrut          = sucursal.getRut();
        int vtelefono        = sucursal.getTelefono();
        int vtelefono1       = sucursal.getTelefono1();
        String vdireccion    = sucursal.getDireccion();
        String vemail        = sucursal.getEmail();
        String vhoraApertura = sucursal.getHoraApertura();
        String vhoraCierre   = sucursal.getHoraCierre();

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_sucursal_adm.sp_new_sucursal(?,?,?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setString(2, vnombre);
            cs.setString(3, vrut);
            cs.setInt   (4, vtelefono);
            cs.setInt   (5, vtelefono1);
            cs.setString(6, vdireccion);
            cs.setString(7, vemail);
            cs.setString(8, vhoraApertura);
            cs.setString(9, vhoraCierre);
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
        
    };
    
    public int editarSucursal(int id,Sucursal sucursal){
        Conector conector = new Conector();
        Connection con       = conector.connectar();
        int cod_error        = 1122; //error en procedimiento
        String vnombre       = sucursal.getNombre();
        int vtelefono        = sucursal.getTelefono();
        int vtelefono1       = sucursal.getTelefono1();
        String vdireccion    = sucursal.getDireccion();
        String vemail        = sucursal.getEmail();
        String vhoraApertura = sucursal.getHoraApertura();
        String vhoraCierre   = sucursal.getHoraCierre();

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_sucursal_adm.sp_edit_sucursal(?,?,?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, id);
            cs.setString(3, vnombre);
            cs.setInt   (4, vtelefono);
            cs.setInt   (5, vtelefono1);
            cs.setString(6, vdireccion);
            cs.setString(7, vemail);
            cs.setString(8, vhoraApertura);
            cs.setString(9, vhoraCierre);
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
           
    
    };
    
    public int eliminarSucursal(int idSucursal){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = idSucursal;
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_sucursal_adm.sp_del_sucursal(?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }

    

    
}
