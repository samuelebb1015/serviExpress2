
package serviexpress.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;

public class Cliente {
    
//Declaracion de variables para Cliente 
private int             id;
private String          rut;
private String          primerNombre;
private String          segundoNombre;
private String          apellidoMaterno;
private String          apellidoPaterno;
private int             telefono;
private String          direccion;
private String          email;
private String          username;
private String          password;
private int             sucursal;
private int         estado;
private int         fiado;

//COnstructor sin parametros
    public Cliente() {
    }

    //Constructor con parametros
    public Cliente(int id, String rut, String primerNombre, String segundoNombre, String apellidoMaterno, String apellidoPaterno, int telefono, String direccion, String email, String username, String password, int sucursal, int estado, int fiado) {
        this.id = id;
        this.rut = rut;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.apellidoMaterno = apellidoMaterno;
        this.apellidoPaterno = apellidoPaterno;
        this.telefono = telefono;
        this.direccion = direccion;
        this.email = email;
        this.username = username;
        this.password = password;
        this.sucursal = sucursal;
        this.estado = estado;
        this.fiado = fiado;
    }

    public int getFiado() {
        return fiado;
    }

    public void setFiado(int fiado) {
        this.fiado = fiado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", rut=" + rut + ", primerNombre=" + primerNombre + ", segundoNombre=" + segundoNombre + ", apellidoMaterno=" + apellidoMaterno + ", apellidoPaterno=" + apellidoPaterno + ", telefono=" + telefono + ", direccion=" + direccion + ", email=" + email + ", username=" + username + ", password=" + password + ", sucursal=" + sucursal + ", estado=" + estado + ", fiado=" + fiado + '}';
    }

    //LOGIN
      public Cliente loginCliente(String username, String password){
        try {
                ArrayList<Cliente> lista = new ArrayList<>();
                Cliente cliente = new Cliente();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_cliente_adm.sp_login_client(?,?,?) }");
        cs.setString(1, username);
        cs.setString(2, password);
        cs.registerOutParameter(3, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(3);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet

                // Seteo de atributos del Objeto Condominio
                cliente.setId(rs.getInt(1));
                cliente.setRut(rs.getString(2));
                cliente.setPrimerNombre(rs.getString(3));
                cliente.setSegundoNombre(rs.getString(4));
                cliente.setApellidoMaterno(rs.getString(5));
                cliente.setApellidoPaterno(rs.getString(6));
                cliente.setTelefono(rs.getInt(7));
                cliente.setDireccion(rs.getString(8));
                cliente.setEmail(rs.getString(9));
                cliente.setUsername(rs.getString(10));
                cliente.setPassword(rs.getString(11));
                cliente.setSucursal(rs.getInt(12));
                cliente.setEstado(rs.getInt(14));
                cliente.setFiado(rs.getInt(15));
    } // Cierre ciclo while
           
            return cliente;
            
            } catch (SQLException ex) {
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };
    // buscar por id
    // listar todos
      // listar todos
    
     public ArrayList<Cliente> listarClientes(int idSuc){
         try {
                ArrayList<Cliente> lista = new ArrayList<>();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_cliente_adm.sp_list_clientes(?,?) }");
        cs.setInt(1, idSuc);
        cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(2);
            
            while(rs.next()){ // se ejecuta un ciclo que rerrore la variable ResultSet
                Cliente cliente = new Cliente();// Se crea el objeto del tipo
                
// Seteo de atributos del Objeto Sucursal
                
                cliente.setId(rs.getInt(1));
                cliente.setRut(rs.getString(2));                
                cliente.setPrimerNombre(rs.getString(3));
                cliente.setSegundoNombre(rs.getString(4));
                cliente.setApellidoPaterno(rs.getString(5));
                cliente.setApellidoMaterno(rs.getString(6));
                cliente.setDireccion(rs.getString(7));
                cliente.setEmail(rs.getString(8));
                cliente.setTelefono(rs.getInt(9));
                cliente.setSucursal(rs.getInt(10));
                cliente.setUsername(rs.getString(11));
                cliente.setEstado(rs.getInt(12));
                cliente.setFiado(rs.getInt(13));
                                   
            
                lista.add(cliente);
    } // Cierre ciclo while
            
            return lista;
            
            } catch (SQLException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    };
     
     
     
    // crear cliente
         public int crearCliente(Cliente cliente){
        Conector conector   = new Conector();
        Connection con      = conector.connectar();
        
        int cod_error       = 1122;//Codigo ERROR: error ejecucion del procedimiento almacenado
        
        //extraccion de datos del objeto enviado        
        int vsucursal                   = cliente.getSucursal();
        String vrut                     = cliente.getRut();
        String vprimernombre            = cliente.getPrimerNombre();
        String vsegundonombre           = cliente.getSegundoNombre();
        String vapellidopaterno         = cliente.getApellidoPaterno();
        String vapellidomaterno         = cliente.getApellidoMaterno();
        int vtelefono                   = cliente.getTelefono();
        String vdireccion               = cliente.getDireccion();
        String vemail                       = cliente.getEmail();
        int vfiado        = cliente.getFiado();
        String vusername        = cliente.getUsername();
        String vpassword        = cliente.getPassword();     
        
             
        

        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_cliente_adm.sp_new_cliente(?,?,?,?,?,?,?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vsucursal);
            cs.setString(3, vrut);
            cs.setString(4, vprimernombre);
            cs.setString(5, vsegundonombre);
            cs.setString(6, vapellidopaterno);
            cs.setString(7, vapellidomaterno);
            cs.setInt(8, vtelefono);
            cs.setString(9, vdireccion);
            cs.setString(10, vemail);
            cs.setInt(11, vfiado);  
            cs.setString(12, vusername);
            cs.setString(13, vpassword);                   
            cs.executeQuery();
            cod_error = cs.getInt(1);           
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
        
    };
     
         
    public Cliente buscarPorID(int id, int idSuc){
        try {
                ArrayList<Cliente> lista = new ArrayList<>();
                Cliente cliente = new Cliente();
                Conector conector = new Conector();
                Connection con = conector.connectar();
        
               
        
        CallableStatement cs = con.prepareCall("{ call pkg_cliente_adm.sp_find_cliente_id(?,?,?) }");
        cs.setInt(1, idSuc);
        cs.setInt(2, id);        
        cs.registerOutParameter(3, oracle.jdbc.internal.OracleTypes.CURSOR);
        cs.executeQuery();
        ResultSet rs = (ResultSet) cs.getObject(3);
        
            
            while(rs.next()){ // se ejecuta un ciclo que recorre la variable ResultSet

                // Seteo de atributos del Objeto Cliente
                cliente.setId(rs.getInt(1));
                cliente.setRut(rs.getString(2));                
                cliente.setPrimerNombre(rs.getString(3));
                cliente.setSegundoNombre(rs.getString(4));
                cliente.setApellidoPaterno(rs.getString(5));
                cliente.setApellidoMaterno(rs.getString(6));
                cliente.setDireccion(rs.getString(7));
                cliente.setEmail(rs.getString(8));
                cliente.setTelefono(rs.getInt(9));
                cliente.setSucursal(rs.getInt(10));
                cliente.setUsername(rs.getString(11));
                cliente.setEstado(rs.getInt(12));
                cliente.setFiado(rs.getInt(13));                                          
            
    } // Cierre ciclo while
           
            return cliente;
            
            } catch (SQLException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    };
    
    public int editarCliente(int id, Cliente cliente){
        Conector conector = new Conector();
        Connection con       = conector.connectar();
        int cod_error        = 1122; //error en procedimiento
        int vsucursal       = cliente.getSucursal();
        String vprimernombre       = cliente.getPrimerNombre();
        String vsegundonombre       = cliente.getSegundoNombre();
        String vapellidopaterno      = cliente.getApellidoPaterno();
        String vapellidomaterno      = cliente.getApellidoMaterno();
        int vtelefono        = cliente.getTelefono();
        String vdireccion    = cliente.getDireccion();
        String vemail        = cliente.getEmail();  

        try {
            
           CallableStatement cs = con.prepareCall("{ call pkg_cliente_adm.sp_edit_cliente(?,?,?,?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vsucursal);
            cs.setInt(3, id);
            cs.setString(4, vprimernombre);
            cs.setString(5, vsegundonombre);
            cs.setString(6, vapellidopaterno);
            cs.setString(7, vapellidomaterno);
            cs.setInt(8, vtelefono);
            cs.setString(9, vdireccion);
            cs.setString(10, vemail);                              
            cs.executeQuery();
            cod_error = cs.getInt(1);           
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
           
    
    };
     
    //cambiar password    
     
     
     
     
    //editar fiado
     
     public int editarFiado(int id, int idSuc){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
        int vsucursalid     = idSuc;
        
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_cliente_adm.sp_change_fiado(?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, vsucursalid);
            cs.setInt   (3, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }
     
     
    // eliminar cliente
     
     public int bajaCliente(int id, int idSuc){
      Conector conector = new Conector();
        Connection con      = conector.connectar();
        int cod_error       = 1122;//error en procedimiento
        int vid             = id;
        int vsucursalid     = idSuc;
        
       
        try {
            
            CallableStatement cs = con.prepareCall("{ call pkg_cliente_adm.sp_change_state(?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt   (2, vsucursalid);
            cs.setInt   (3, vid);
            
            cs.executeQuery();
            cod_error = cs.getInt(1);
            
            
            return cod_error;
            
          
        } catch (SQLException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }


}
