/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.persistencia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;
import serviexpress.conexion.Conector;

/**
 *
 * @author jhan
 */
public class Producto {

    private int id;
    private String nombre;
    private String descripcion;
    private int precioVenta;
    private int stock, stockCritico;
    private int idProveedor;
    private int idCategoria;
    private String vencimiento;

    public Producto() {
    }

    public Producto(int id, String nombre, String descripcion, int precioVenta, int stock, int stockCritico, int idProveedor, int idCategoria, String vencimiento) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precioVenta = precioVenta;
        this.stock = stock;
        this.stockCritico = stockCritico;
        this.idProveedor = idProveedor;
        this.idCategoria = idCategoria;
        this.vencimiento = vencimiento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(int precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStockCritico() {
        return stockCritico;
    }

    public void setStockCritico(int stockCritico) {
        this.stockCritico = stockCritico;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(String vencimiento) {
        this.vencimiento = vencimiento;
    }

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precioVenta=" + precioVenta + ", stock=" + stock + ", stockCritico=" + stockCritico + ", idProveedor=" + idProveedor + ", idCategoria=" + idCategoria + ", vencimiento=" + vencimiento + '}';
    }

    public ArrayList<Producto> listarProductos() {
        try {
            ArrayList<Producto> lista = new ArrayList<>();
            Conector conector = new Conector();
            Connection con = conector.connectar();

            CallableStatement cs = con.prepareCall("{ call pkg_producto_adm.sp_list_productos(?) }");
            cs.registerOutParameter(1, oracle.jdbc.internal.OracleTypes.CURSOR);
            cs.executeQuery();
            ResultSet rs = (ResultSet) cs.getObject(1);

            while (rs.next()) { // se ejecuta un ciclo que rerrore la variable ResultSet
                Producto producto = new Producto();// Se crea el objeto del tipo

// Seteo de atributos del Objeto Sucursal
                producto.setId(rs.getInt(1));
                producto.setNombre(rs.getString(2));
                producto.setDescripcion(rs.getString(3));
                producto.setPrecioVenta(rs.getInt(4));
                producto.setStock(rs.getInt(5));
                producto.setStockCritico(rs.getInt(6));
                producto.setIdProveedor(rs.getInt(7));
                producto.setIdCategoria(rs.getInt(8));
                producto.setVencimiento(rs.getString(9));

                lista.add(producto);
            } // Cierre ciclo while

            return lista;

        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    ;
     
     
     
    // crear cliente
         public int crearProducto(Producto producto) {
        Conector conector = new Conector();
        Connection con = conector.connectar();

        int cod_error = 1122;//Codigo ERROR: error ejecucion del procedimiento almacenado

        //extraccion de datos del objeto enviado        
        int vid = producto.getId();
        String vnombre = producto.getNombre();
        String vdescripcion = producto.getDescripcion();
        int vprecioventa = producto.getPrecioVenta();
        int vstock = producto.getStock();
        int vstockcritico = producto.getStockCritico();
        int vcategoria = producto.getIdCategoria();
        int vproveedor = producto.getIdProveedor();
        String vvencimiento = producto.getVencimiento();

        try {

            CallableStatement cs = con.prepareCall("{ call pkg_producto_adm.sp_new_producto(?,?,?,?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vid);
            cs.setString(3, vnombre);
            cs.setString(4, vdescripcion);
            cs.setInt(5, vprecioventa);
            cs.setInt(6, vstock);
            cs.setInt(7, vstockcritico);
            cs.setInt(8, vcategoria);
            cs.setInt(9, vproveedor);
            cs.setString(10, vvencimiento);

            cs.executeQuery();
            cod_error = cs.getInt(1);

            return cod_error;

        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;

    }

    ;
     
         
    public Producto buscarPorID(int id) {
        try {
            ArrayList<Producto> lista = new ArrayList<>();
            Producto producto = new Producto();
            Conector conector = new Conector();
            Connection con = conector.connectar();

            CallableStatement cs = con.prepareCall("{ call pkg_producto_adm.sp_find_producto_id(?,?) }");
            cs.setInt(1, id);
            cs.registerOutParameter(2, oracle.jdbc.internal.OracleTypes.CURSOR);
            cs.executeQuery();
            ResultSet rs = (ResultSet) cs.getObject(2);

            while (rs.next()) { // se ejecuta un ciclo que recorre la variable ResultSet

                // Seteo de atributos del Objeto Cliente
                producto.setId(rs.getInt(1));
                producto.setNombre(rs.getString(2));
                producto.setDescripcion(rs.getString(3));
                producto.setPrecioVenta(rs.getInt(4));
                producto.setStock(rs.getInt(5));
                producto.setStockCritico(rs.getInt(6));
                producto.setIdProveedor(rs.getInt(7));
                producto.setIdCategoria(rs.getInt(8));
                producto.setVencimiento(rs.getString(9));

            } // Cierre ciclo while

            return producto;

        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    ;
    
    public int editarProducto(int id, Producto producto) {
        Conector conector = new Conector();
        Connection con = conector.connectar();
        int cod_error = 1122; //error en procedimiento
        int vid = producto.getId();
        String vnombre = producto.getNombre();
        String vdescripcion = producto.getDescripcion();
        int vprecioventa = producto.getPrecioVenta();
        int vstockcritico = producto.getStockCritico();
        String vvencimiento = producto.getVencimiento();

        try {

            CallableStatement cs = con.prepareCall("{ call pkg_producto_adm.sp_edit_producto(?,?,?,?,?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vid);
            cs.setString(3, vnombre);
            cs.setString(4, vdescripcion);
            cs.setInt(5, vprecioventa);
            cs.setInt(6, vstockcritico);
            cs.setString(7, vvencimiento);

            cs.executeQuery();
            cod_error = cs.getInt(1);

            return cod_error;

        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;

    }

    ;
     
    //eliminar producto

public int eliminarProducto(int id) {
        Conector conector = new Conector();
        Connection con = conector.connectar();
        int cod_error = 1122;//error en procedimiento
        int vid = id;

        try {

            CallableStatement cs = con.prepareCall("{ call pkg_producto_adm.sp_del_producto(?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vid);

            cs.executeQuery();
            cod_error = cs.getInt(1);

            return cod_error;

        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }

    //editar fiado
    public int bajarStock(int id, int cantidad) {
        Conector conector = new Conector();
        Connection con = conector.connectar();
        int cod_error = 1122;//error en procedimiento
        int vid = id;
        int vcantidad = cantidad;

        try {

            CallableStatement cs = con.prepareCall("{ call pkg_producto_adm.sp_bajar_stock(?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vid);
            cs.setInt(3, vcantidad);

            cs.executeQuery();
            cod_error = cs.getInt(1);

            return cod_error;

        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }

    public int subirStock(int id, int cantidad) {
        Conector conector = new Conector();
        Connection con = conector.connectar();
        int cod_error = 1122;//error en procedimiento
        int vid = id;
        int vcantidad = cantidad;

        try {

            CallableStatement cs = con.prepareCall("{ call pkg_producto_adm.sp_subir_stock(?,?,?) }");
            cs.registerOutParameter(1, OracleTypes.INTEGER);
            cs.setInt(2, vid);
            cs.setInt(3, vcantidad);

            cs.executeQuery();
            cod_error = cs.getInt(1);

            return cod_error;

        } catch (SQLException ex) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod_error;
    }

}
