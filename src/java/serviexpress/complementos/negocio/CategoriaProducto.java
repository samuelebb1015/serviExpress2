/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviexpress.complementos.negocio;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import serviexpress.conexion.Conector;

/**
 *
 * @author jhan
 */
public class CategoriaProducto {

    private int id;
    private String nombre;
    private String descripcion;

    public CategoriaProducto() {
    }

    public CategoriaProducto(int id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "CategoriaProducto{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + '}';
    }

    public ArrayList<CategoriaProducto> listarCategorias() {
        try {
            ArrayList<CategoriaProducto> lista = new ArrayList<>();
            Conector conector = new Conector();
            Connection con = conector.connectar();

            CallableStatement cs = con.prepareCall("{ call pkg_categoria_producto_adm.sp_list_categorias(?) }");
            cs.registerOutParameter(1, oracle.jdbc.internal.OracleTypes.CURSOR);
            cs.executeQuery();
            ResultSet rs = (ResultSet) cs.getObject(1);

            while (rs.next()) { // se ejecuta un ciclo que rerrore la variable ResultSet
                CategoriaProducto cat = new CategoriaProducto();// Se crea el objeto del tipo

// Seteo de atributos del Objeto Categoria
                cat.setId(rs.getInt(1));
                cat.setNombre(rs.getString(2));
                cat.setDescripcion(rs.getString(3));

                lista.add(cat); //se carga el objeto seteado en el ciclo

            } // Cierre ciclo while

            return lista;

        } catch (SQLException ex) {
            Logger.getLogger(CategoriaProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
;

}
