<%-- 
    Document   : registrarProducto
    Created on : 27-jun-2018, 2:17:08
    Author     : Samuel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<%@page import="serviexpress.complementos.negocio.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Agregar Producto</h2>
        <form action="registroProductoController" method="POST" >

            Nombre 
            <input type="text" name="txtNombre" required/><br>
            Descripción 
            <textarea name="txtDescripcion" rows="10" cols="40"></textarea><br>
            Precio Venta 
            <input type="text" name="txtPrecio"><br>
            Stock 
            <input type="text" name="txtStock"><br>
            Stock Crítico 
            <input type="text" name="txtCritico"><br>            

            Categoría:
            <select name="categoria">
                <% CategoriaProducto categoria = new CategoriaProducto();
                    ArrayList<CategoriaProducto> cat = categoria.listarCategorias();
                    if (cat != null) {
                        for (CategoriaProducto cats : cat) {%>
                <option value="<%=cats.getId()%>"><%=cats.getNombre()%></option>
                <% }
                    } %>                  
            </select>
            <br />
            Proveedor:
            <select name="proveedor">
                <% Proveedor proveedor = new Proveedor();
                    ArrayList<Proveedor> pro = proveedor.listarProveedores();
                    if (pro != null) {
                        for (Proveedor prov : pro) {%>
                <option value="<%=prov.getId()%>"><%=prov.getNombre()%></option>
                <% }
                    }%>    
            </select>
            <br />

            <input type="submit" name="btnRegistrar" value="Registrar">


        </form>
            
            <%
                       
            out.print("<br>");
            out.print(request.getAttribute("resultado"));

            if (request.getAttribute("resultado")=="0") {
            out.print("<br><h2> Registrado Exitosamente</h2>");
            }

            if (request.getAttribute("resultado")=="1") {
            out.print("<br><h2>ERROR : Cliente No se pudo registrar");
            }

            %>
    </body>
</html>
