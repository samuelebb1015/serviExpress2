<%-- 
    Document   : generarOrdenDetalle
    Created on : 02-jul-2018, 20:42:55
    Author     : Samuel
--%>

<%@page import="serviexpress.persistencia.Pedido"%>
<%@page import="java.util.ArrayList"%>
<%@page import="serviexpress.persistencia.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>  
        <form action="generarOrdenDetalleController" method="POST" >
        <%   
            
            int idProve = Integer.parseInt(request.getParameter("proveedor")); 
            Pedido pedidoId = new Pedido();
            ArrayList <Pedido> lista = pedidoId.listarOrdenesPedidos();
            int ultimo = 0;
            for(Pedido p : lista){
                if(p.getId()>=ultimo){
                    ultimo = p.getId();
                }
            }
            
        %>
        <h1>Generar Orden para<% %></h1>

        Orden N°: 
        <input type="text" value =<%=ultimo %> name="txtOrden" readonly="readonly"><br>
        Proveedor ID:
        <input type="text" value =<%=idProve %> name="proveedor" readonly="readonly"><br>
        Producto:
        <select name="producto">
            <% Producto producto = new Producto();
                ArrayList<Producto> pro = producto.listarProductos();
                if (pro != null) {
                    for (Producto prod : pro) {
                        if (prod.getIdProveedor() == idProve) {
            %>
            <option value="<%=prod.getId()%>"><%=prod.getNombre()%></option>
            <% }
                    }
                }%>    
        </select>
        <br />     
        
        Cantidad: 
        <input type="text" name="txtCantidad"><br>
        
        <input type="submit" name="btnAgregar" value="Agregar"><br/>
        <a href="generarOrden.jsp">Volver</a>
        
        
        </form>  
              
    </body>
</html>
