<%-- 
    Document   : listarDetalle
    Created on : 03-jul-2018, 5:57:13
    Author     : Samuel
--%>

<%@page import="serviexpress.complementos.negocio.DetallePedido"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page language="java"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="serviexpress.persistencia.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listar Detalle</title>
    </head>
    <%
        int idOrden = 0;

        if (request.getParameter("idPro") != null) {
            idOrden = Integer.parseInt(request.getParameter("idPro"));
        }

        String estado = "";

        DetallePedido det = new DetallePedido();
        Producto pro = new Producto();
        int estadoDetalle = 0;

        if (request.getParameter("idOrden") != null) {

            //int idDetalle = Integer.parseInt(request.getParameter("idDet"));
            //int idProducto = Integer.parseInt(request.getParameter("idProd"));
            //int cantidad = Integer.parseInt(request.getParameter("cantidad"));            
            //det.confirmarPedido(idDetalle);
            idOrden = Integer.parseInt(request.getParameter("idOrden"));
        }

        if (request.getParameter("idDet") != null) {
            int idDetalle = Integer.parseInt(request.getParameter("idDet"));
            det.confirmarPedido(idDetalle);
            estadoDetalle = 1;
        }

        if (request.getParameter("idProd") != null && estadoDetalle==1) {
            int idProducto = Integer.parseInt(request.getParameter("idProd"));
            int cantidad = Integer.parseInt(request.getParameter("cant"));
            pro.subirStock(idProducto, cantidad);
        }


    %>

    <body>
        <table align="center" border="1px" width="80%">
            <tr>
                <th>ID Orden</th>
                <th>Producto ID</th>
                <th>Cantidad</th>
                <th>Fecha/Hora</th>
                <th>Estado</th>
                <th>Acción</th>
            </tr>
            <%                ArrayList<DetallePedido> lista = det.buscarPorID(idOrden);
                for (DetallePedido p : lista) {
                    if (p.getEstado() == 1) {
                        estado = "Confirmado";
                    }else{
                        estado = "Pendiente";
                    }
            %>

            <tr>
                <td><%=p.getIdOrden()%> </td>
                <td><%=p.getIdProd()%> </td>
                <td><%=p.getCantidad()%> </td>
                <td><%=p.getFecha()%> </td>                    
                <td><%= estado%></td>
                <td><a href="listarDetalle.jsp?idOrden=<%=p.getIdOrden()%>&idProd=<%=p.getIdProd()%>&cant=<%=p.getCantidad()%>&idDet=<%=p.getId()%>&cant=<%=p.getCantidad()%>">Confirmar Detalle</a></td>
            </tr>
            <%
                }

            %>
        </table>
        <a href="listarOrdenes.jsp">Volver</a>

    </body>
</html>

