<%@page import="serviexpress.persistencia.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="serviexpress.complementos.negocio.TipoEmpleado"%>
<%@page import="serviexpress.persistencia.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Editar Cliente</h1>

        <form action="editarClienteController" method="POST" >             

            <table align="center" border="1px" width="80%">

                <%
                   int codigo = 0;
                    
                    if(request.getParameter("idCli2") != null){
                        codigo = Integer.parseInt(request.getParameter("idCli2"));
                    }
                    
                    String t = "Activo";
                    Cliente cliente = new Cliente();
                    
                    if (request.getParameter("fiadoId") != null) {
                        int codii = Integer.parseInt(request.getParameter("fiadoId"));
                        cliente.editarFiado(codii, 1);
                        codigo = Integer.parseInt(request.getParameter("fiadoId"));}
                    
                    if (request.getParameter("estadoId") != null) {
                        int estado = Integer.parseInt(request.getParameter("estadoId"));
                        cliente.bajaCliente(estado, 1);
                        codigo = Integer.parseInt(request.getParameter("estadoId"));}
                    
                    cliente = cliente.buscarPorID(codigo, 1);                    
                        
                        if (cliente.getEstado() == 0) {
                            t = "Inactivo";
                        }


                %>                   

                <tr>
                    <th>ID</th>
                    <th>Rut</th>
                    <th>Primer Nombre</th>
                    <th>Segundo Nombre</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Dirección</th>
                    <th>Email</th>
                    <th>Teléfono</th>
                    <th>Sucursal ID</th>
                    <th>Username</th>
                    <th>Estado</th>                  
                    <th>Fiado</th> 

                </tr>           



                <tr>
                    <td><input type="text" name="txtId" value="<%=cliente.getId()%>" /></td>
                    <td><%= cliente.getRut()%> </td>
                    <td><input type="text" name="txtNombre1" value="<%=cliente.getPrimerNombre()%>" required/></td>
                    <td><input type="text" name="txtNombre2" value="<%=cliente.getSegundoNombre()%>" required/></td>
                    <td><input type="text" name="txtPaterno" value="<%=cliente.getApellidoPaterno()%>" required></td>
                    <td><input type="text" name="txtMaterno" value="<%=cliente.getApellidoMaterno()%>" required></td>
                    <td><input type="text" name="txtDireccion" value="<%=cliente.getDireccion()%>" required></td>
                    <td><input type="text" name="txtEmail" value="<%=cliente.getEmail()%>" required=""></td>
                    <td><input type="text" name="txtTelefono" value="<%=cliente.getTelefono()%>" required></td>
                    <td><input type="text" name="txtSuc" value="<%=cliente.getSucursal()%>" required/></td>
                    <td><%= cliente.getUsername()%> </td>
                    <%  if (cliente.getEstado() == 1) {%>
                    <td><a href="editarClientes.jsp?estadoId=<%=cliente.getId()%>">Dar de Baja</a></td>                 
                    <% } else { %>
                    <td><a href="editarClientes.jsp?estadoId=<%=cliente.getId()%>">Reincorporar</a></td>
                    <% } %>
                    <%  if (cliente.getFiado() == 1) {%>
                    <td><a href="editarClientes.jsp?fiadoId=<%=cliente.getId()%>">Desactivar</a></td>                 
                    <% } else { %>
                    <td><a href="editarClientes.jsp?fiadoId=<%=cliente.getId()%>">Activar</a></td>
                    <% } %>
                </tr>
            </table>  
            <%

                    //request.getRequestDispatcher("listarClientes.jsp");
                
            %>



            <input type="submit" name="btnEditar" value="Editar">

        </form>

        <%
            out.print("<br>");
            out.print(request.getAttribute("resultado"));

            if (request.getAttribute("resultado") == "0") {
                out.print("<br><h2>Cliente Editado Exitosamente</h2>");
            }

            if (request.getAttribute("resultado") == "1") {
                out.print("<br><h2>ERROR : Empleado No se pudo Editar");
            }
        %>

    </body>

</html>