<%@page import="java.util.ArrayList"%>
<%@page import="serviexpress.complementos.negocio.TipoEmpleado"%>
<%@page import="serviexpress.persistencia.Empleado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Editar Empleado</h1>

        <form action="editarEmpleadoController" method="POST" >             

            <table align="center" border="1px" width="80%">
            <tr>
                <th>ID</th>
                <th>Rut</th>
                <th>Primer Nombre</th>
                <th>Segundo Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Dirección</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Sucursal ID</th>
                <th>Username</th>
                <th>Tipo Empleado</th> 
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>           
            
            
            <%
                int codigo = Integer.parseInt(request.getParameter("idEmp"));
                String t="";                
                Empleado empleado = new Empleado();                
                empleado = empleado.buscarPorID(codigo,1);

                   if(empleado.getTipo()==1){
                       t ="Administrador";
                  }if(empleado.getTipo()==2){
                       t = "Mecánico";
                  }if(empleado.getTipo()==3){
                      t="Cajero";
                  };
                  
                 

                  %>     
                <tr>
                    <td><input type="text" name="txtId" value="<%=empleado.getId()%>" /></td>
                    <td><%= empleado.getRut()%> </td>
                    <td><input type="text" name="txtNombre1" value="<%=empleado.getPrimerNombre()%>" required/></td>
                    <td><input type="text" name="txtNombre2" value="<%=empleado.getSegundoNombre()%>" required/></td>
                    <td><input type="text" name="txtPaterno" value="<%=empleado.getApellidoPaterno()%>" required></td>
                    <td><input type="text" name="txtMaterno" value="<%=empleado.getApellidoMaterno()%>" required></td>
                    <td><input type="text" name="txtDireccion" value="<%=empleado.getDireccion()%>" required></td>
                    <td><input type="text" name="txtEmail" value="<%=empleado.getEmail()%>" required=""></td>
                    <td><input type="text" name="txtTelefono" value="<%=empleado.getTelefono()%>" required></td>
                    <td><input type="text" name="txtSuc" value="<%=empleado.getSucursal()%>"/></td>
                    <td><%= empleado.getUsername()%> </td>
                    <td><%= empleado.getTipo() %> </td>
                    <td><%= t %> </td>                    
                    <td></td>
                </tr>
               </table>        
        <input type="submit" name="btnEditar" value="Editar">
        </form>
                
                    <%
            out.print("<br>");
            out.print(request.getAttribute("resultado"));

            if (request.getAttribute("resultado") == "0") {
                out.print("<br><h2>Cliente Editado Exitosamente</h2>");
            }

            if (request.getAttribute("resultado") == "1") {
                out.print("<br><h2>ERROR : Empleado No se pudo Editar");
            }
        %>
                    
    </body>
    
</html>
